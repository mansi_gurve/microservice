package com.java.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.Dto.UserRequestDto;
import com.java.entity.User;
import com.java.exception.UserAlreadyExistException;
import com.java.exception.UserNotFoundException;
import com.java.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Override
	public boolean register(UserRequestDto userDto) {
		
		User user = new User();
		BeanUtils.copyProperties(userDto, user);
		Optional<User> userByName = userRepository.findByName(user.getName());
		System.out.println(userByName);
		if(userByName.isPresent()) {
			System.out.println("if");
		
			return false;
		}
	
		else {
			System.out.println("else");
			userRepository.save(user);
			return true;
		}
		
	}

	@Override
	public boolean getUserById(Integer userId) {
		Optional<User> userById=userRepository.findById(userId);
		if(userById.isPresent())
			return true;
		else 
			return false;
	}
	

}
