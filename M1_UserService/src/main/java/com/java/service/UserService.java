package com.java.service;

import com.java.Dto.UserRequestDto;


public interface UserService {

	boolean register(UserRequestDto userDto);
	//void register1(UserRequestDto userDto);

	boolean getUserById(Integer userId);

}
