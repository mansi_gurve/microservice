package com.java.feign;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.java.Dto.AccountRequestDto;
import com.java.Dto.AccountResponseDto;

//@FeignClient("ACCOUNTSERVICE")
@FeignClient(name="ACCOUNTSERVICE",url="http://localhost:8082")
public interface AccountClient {

	@PostMapping("/account")
	public ResponseEntity<String> createAccount(@Valid @RequestBody AccountRequestDto responsedto);

	@GetMapping("/account/{userId}")
	public ResponseEntity<AccountResponseDto> accountDetail(@PathVariable Integer userId);
}