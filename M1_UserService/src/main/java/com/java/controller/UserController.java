package com.java.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.java.Dto.AccountRequestDto;
import com.java.Dto.AccountResponseDto;
import com.java.Dto.UserRequestDto;
import com.java.exception.UserAlreadyExistException;
import com.java.feign.AccountClient;
import com.java.service.UserService;

@RestController
@Validated
public class UserController {

	@Autowired
	UserService userService;
	@Autowired
	AccountClient accountClient;

	@PostMapping("/user/register")
	public ResponseEntity<String> registerUser(@Valid @RequestBody UserRequestDto userDto) {
		//System.out.print("c"+userDto);
		boolean result = userService.register(userDto);
		 userService.register(userDto);
		if (result==true)
			return new ResponseEntity<String>("User Registered Sucessfully", HttpStatus.OK);
		else
			throw new UserAlreadyExistException("User with "+userDto.getName()+" name already exist");
	}
	
	@GetMapping("/user/{userId}")
	public boolean isUser(@PathVariable Integer userId) {
		
		return userService.getUserById(userId);
		 
	}
	
	@PostMapping("/user/account")
	public ResponseEntity<String> createAccount(@Valid @RequestBody AccountRequestDto responsedto){
		return accountClient.createAccount(responsedto);
	}

	@GetMapping("/user/account/{userId}")
	public ResponseEntity<AccountResponseDto> accountDetail(@PathVariable Integer userId){
		return accountClient.accountDetail(userId);
	}

	
	
}
