package com.java.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.java.entity.User;
@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

	Optional<User> findByName(String name);

}
