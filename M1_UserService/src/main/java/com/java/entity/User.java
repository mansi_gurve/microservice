package com.java.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Data;

@Entity
@Data
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_post")
	@SequenceGenerator(name = "seq_post", initialValue = 101)
	private Integer userId;
	private String name;
	private long phNumber;
	private String address;

	public Integer getUserId() {
		return userId;
	}

	public String getName() {
		return name;
	}

	public long getPhNumber() {
		return phNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPhNumber(long phNumber) {
		this.phNumber = phNumber;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", name=" + name + ", phNumber=" + phNumber + ", address=" + address + "]";
	}

}
