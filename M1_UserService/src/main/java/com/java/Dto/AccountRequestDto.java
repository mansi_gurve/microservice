package com.java.Dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public class AccountRequestDto {
    @NotEmpty(message = "Enter account type")
	private String accountType;
    @Min(value=1000, message="Minimum balance must be equal or greater than 1000")  
   
	private double balance;
    @Min(value=101, message="must be equal or greater than 101")  
    @Max(value=999, message="must be equal or less than 999")  
	private Integer userId;

	public String getAccountType() {
		return accountType;
	}

	public double getBalance() {
		return balance;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "AccountRequestDto [accountType=" + accountType + ", balance=" + balance + ", userId=" + userId + "]";
	}

	
}
