package com.java.Dto;

public class AccountResponseDto {

	private int accountNumber;
	private String accountType;
	private double balance;
	private Integer userId;

	public int getAccountNumber() {
		return accountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public double getBalance() {
		return balance;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
