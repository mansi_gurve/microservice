package com.java.Dto;

import lombok.Data;

@Data
public class UserRequestDto {
	private Integer userId;
	private String name;
	private long phNumber;
	private String address;

	public Integer getUserId() {
		return userId;
	}

	public String getName() {
		return name;
	}

	public long getPhNumber() {
		return phNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPhNumber(long phNumber) {
		this.phNumber = phNumber;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "UserRequestDto [userId=" + userId + ", name=" + name + ", phNumber=" + phNumber + ", address=" + address
				+ "]";
	}

}
