package com.java.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.java.enitity.Account;
import java.util.Optional;
@Repository
public interface AccountRepository extends CrudRepository<Account, Integer> {

	Optional<Account> findByUserId(Integer userId);

}
