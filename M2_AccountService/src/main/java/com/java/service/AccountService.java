package com.java.service;

import com.java.dto.AccountRequestDto;
import com.java.dto.AccountResponseDto;

public interface AccountService {

	AccountResponseDto getAccountDetailById(Integer userId);

	boolean createAccount(AccountRequestDto responsedto);

}
