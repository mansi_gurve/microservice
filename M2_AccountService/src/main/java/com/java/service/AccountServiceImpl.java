package com.java.service;

import java.util.Random;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.dto.AccountRequestDto;
import com.java.dto.AccountResponseDto;
import com.java.enitity.Account;
import com.java.exception.AccountNotPresentException;
import com.java.exception.UserNotFoundException;
import com.java.feign.UserClient;
import com.java.repository.AccountRepository;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	AccountRepository accountRepository;
	@Autowired
	UserClient userClient;

	@Override
	public AccountResponseDto getAccountDetailById(Integer userId) {

		Optional<Account> account = accountRepository.findByUserId(userId);

		AccountResponseDto responsedto = new AccountResponseDto();
		if (account.isPresent()) {
			// BeanUtils.copyProperties(account, responsedto);
			responsedto.setAccountNumber(account.get().getAccountNumber());
			responsedto.setAccountType(account.get().getAccountType());
			responsedto.setBalance(account.get().getBalance());
			responsedto.setUserId(account.get().getUserId());
			return responsedto;
		} else {
			System.out.println("else");
			throw new AccountNotPresentException("Account with given userId not present");
		}

	}

	@Override
	public boolean createAccount(AccountRequestDto requestdto) {

		boolean result = userClient.getUserById(requestdto.getUserId());

		if (result) {
			Account account = new Account();

			BeanUtils.copyProperties(requestdto, account);
			Random random = new Random();
			account.setAccountNumber(random.nextInt());

			Account a = accountRepository.save(account);
			if (a != null) {
				return true;
			}
		} else 
			throw new UserNotFoundException("User not found with " + requestdto.getUserId() + " Id");
		
		return false;
	}

}
