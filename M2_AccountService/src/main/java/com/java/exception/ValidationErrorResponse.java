package com.java.exception;

import java.util.HashMap;
import java.util.Map;

import org.apache.catalina.valves.ErrorReportValve;

public class ValidationErrorResponse extends ErrorResponse {
	private Map<String, String> errors = new HashMap<String, String>();

	public Map<String, String> getErrors() {
		return errors;
	}

	public void setErrors(Map<String, String> errors) {
		this.errors = errors;
	}

}
