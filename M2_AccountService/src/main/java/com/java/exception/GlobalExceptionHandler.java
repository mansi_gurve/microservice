package com.java.exception;

import java.time.LocalDateTime;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ValidationErrorResponse> handleException(MethodArgumentNotValidException ex) {
		List<FieldError> errors = ex.getFieldErrors();
		ValidationErrorResponse vrs = new ValidationErrorResponse();
		vrs.setDateTime(LocalDateTime.now());
		vrs.setStatusCode(HttpStatus.BAD_REQUEST.value());
		vrs.setMessage("Input Data has some errors");

		for (FieldError fieldError : errors) {
			vrs.getErrors().put(fieldError.getField(), fieldError.getDefaultMessage());
		}
		return new ResponseEntity<ValidationErrorResponse>(vrs, HttpStatus.BAD_REQUEST);
	}
	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<ValidationErrorResponse> handleException(ConstraintViolationException ex){
		ValidationErrorResponse vrs = new ValidationErrorResponse();
		vrs.setDateTime(LocalDateTime.now());
		vrs.setStatusCode(HttpStatus.BAD_REQUEST.value());
		vrs.setMessage("Input Data has some errors");
		/*ex.getConstraintViolations().forEach(error->{
			vrs.getErrors().put("field",error.getMessage());
		});*/
		return new ResponseEntity<ValidationErrorResponse>(vrs, HttpStatus.BAD_REQUEST);
	}
	@ExceptionHandler(AccountNotPresentException.class)
	public ResponseEntity<ErrorResponse> handleException(AccountNotPresentException ex){
		ErrorResponse er=new ErrorResponse();
		er.setMessage(ex.getMessage());
		er.setDateTime(LocalDateTime.now());
		er.setStatusCode(HttpStatus.NOT_FOUND.value());
		return new ResponseEntity<ErrorResponse>(er,HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<ErrorResponse> handleException(UserNotFoundException ex){
		ErrorResponse er=new ErrorResponse();
		er.setMessage(ex.getMessage());
		er.setDateTime(LocalDateTime.now());
		er.setStatusCode(HttpStatus.NOT_FOUND.value());
		return new ResponseEntity<ErrorResponse>(er,HttpStatus.NOT_FOUND);
	}
}
