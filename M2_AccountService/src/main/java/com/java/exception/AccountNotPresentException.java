package com.java.exception;

public class AccountNotPresentException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountNotPresentException(String message) {
		super(message);
	}

}
