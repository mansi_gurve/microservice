package com.java.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "USERSERVICE",url = "http://localhost:8081")
public interface UserClient {
	@GetMapping("/user/{userId}")
	public boolean getUserById(@PathVariable Integer userId);

}
