package com.java.controller;


import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.java.dto.AccountRequestDto;
import com.java.dto.AccountResponseDto;
import com.java.service.AccountService;

@RestController
@Validated
public class AccountController {

	@Autowired
	AccountService accountService;

	@PostMapping("/account")
	public ResponseEntity<String> createAccount(@Valid @RequestBody AccountRequestDto responsedto) {
		boolean result = accountService.createAccount(responsedto);
		if (result)
			return new ResponseEntity<String>("Account created Sucessfully", HttpStatus.OK);
		else
			return new ResponseEntity<String>("Account not created", HttpStatus.NOT_ACCEPTABLE);
		
			
	}

	@GetMapping("/account/{userId}")
	public ResponseEntity<AccountResponseDto> accountDetail( @PathVariable @Min(101) Integer userId) {
		AccountResponseDto dto = accountService.getAccountDetailById(userId);
		
		if (dto != null)
			return new ResponseEntity<AccountResponseDto>(dto, HttpStatus.OK);
		else
			return new ResponseEntity<AccountResponseDto>(dto, HttpStatus.NOT_ACCEPTABLE);
	}
}
